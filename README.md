# Serve

This program serves files on a directory.

Just call `serve` to start serving the files on the current directory.

For more options call `serve -h`.
