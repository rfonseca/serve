package main

import (
	"flag"
	"fmt"
	"log"
	"net"
	"net/http"
	"os"
	"path/filepath"
	"strings"
)

var dir, bind string
var port int

func init() {
	pwd, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}
	flag.IntVar(&port, "port", 8000, "Port")
	flag.StringVar(&dir, "dir", pwd, "Directory")
	flag.StringVar(&bind, "bind", "0.0.0.0", "Bind address")
	flag.Parse()
}

func main() {
	bind := fmt.Sprintf("%s:%d", bind, port)
	dir, err := filepath.Abs(dir)
	if err != nil {
		log.Fatalln("Failed to assert absolute directory:", err)
	}
	log.Printf("Listening on %s\n", bind)
	log.Printf("Serving %s\n", dir)
	for _, ip := range getIPv4IPs() {
		log.Printf("Share link: http://%s:%d/\n", ip, port)
	}
	log.Fatal(http.ListenAndServe(bind, loggingHandler(http.FileServer(http.Dir(dir)))))
}

func loggingHandler(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Printf("%s %s %s\n", r.RemoteAddr, r.Method, r.URL)
		handler.ServeHTTP(w, r)
	})
}

func getIPv4IPs() (ips []string) {
	ifaces, err := net.Interfaces()
	if err != nil {
		panic(err)
	}
	for _, i := range ifaces {
		switch {
		case i.Name == "lo":
			fallthrough
		case strings.HasPrefix(i.Name, "br-"):
			fallthrough
		case strings.HasPrefix(i.Name, "docker"):
			continue
		}
		addrs, err := i.Addrs()
		if err != nil {
			panic(err)
		}

		for _, addr := range addrs {
			var ip net.IP
			switch v := addr.(type) {
			case *net.IPNet:
				ip = v.IP
			case *net.IPAddr:
				ip = v.IP
			}
			if ip.To4() != nil {
				ips = append(ips, ip.String())
			}
		}
	}
	return
}
